all: client server
	cd client && stack build
	cd server && stack build

client:

server:

install: all
	cd server && stack build --copy-bins

run: all
	cd server && stack exec talkmon -- --port 13003
