{-# LANGUAGE Arrows #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}

module Main where

import Input

import Control.Lens ((^.), makeLenses, to)
import Control.Monad (void, filterM, (>=>), liftM)
import Language.Javascript.JSaddle ( js, jsg, jss, jsf, new, obj, fun, val
                                   , array, valToText, strictEqual, (<#))
import qualified JavaScript.Array as Array
import GHCJS.Types (JSVal(..), isUndefined)
import GHCJS.Marshal ()
import GHCJS.Foreign ()
-- import JavaScript.JSON ()
import Control.Monad.IO.Class
import System.Random
import qualified Data.Aeson as Aeson

import Data.IORef


-- | A phaser callback. Takes a phaser instance
type PhaserCallback = JSVal -> IO ()


-- The fixed timestep
timestep :: Float
timestep = 1 / 60


-- | Load data
preload :: JSVal -> IO ()
preload phaser = void $ do
  let loadImage name url = (js "load" . jsf "image" [name, url])
  let loadSpritesheet name url w h = (js "load" . jsf "spritesheet" [val name, val url, val (w::Int), val (h::Int)])

  phaser ^. loadImage "robot" "assets/sprites/robo.png"

  return ()


-- | Create game
create :: JSVal -> IO ()
create phaser = do
  let add = (+)
  let sub = subtract
  let addSprite x y image = (js "add" . jsf "sprite" [val (x::Int), val (y::Int), val image])
  let setAnchor f = (js "anchor" . jsf "set" [(f::Float)])
  let addAnimation name frames = (js "animations" . jsf "add" [val name, val (frames::[Int])])
  let playAnimation name framerate loop = (js "animations" . jsf "play" [val name, val (framerate::Int), val (loop::Bool)])

  phaser ^. js "scale" ^. jss "scaleMode" (jsg "Phaser" ^. js "ScaleManager" ^. js "RESIZE")

  bot <- phaser ^. addSprite 500 500 "robot"
  bot ^. setAnchor 0.5

  return ()


-- | Update game
update :: JSVal -> IO ()
update phaser = do
  return ()


-- | Render game
render :: JSVal -> IO ()
render phaser = do
  return ()


-- | Initialise phaser
initPhaser :: JSVal -> IO ()
initPhaser element = do
  -- Initialise phaser
  phaser <- new ( jsg "Phaser" ^. js "Game" )
                ( "100%"
                , "100%"
                , jsg "Phaser" ^. js "AUTO"
                , element
                , obj >>= \cb -> do
                    cb <# "preload" $ fun (\_ _ [p] -> preload p)
                    cb <# "create"  $ fun (\_ _ [p] -> create p)
                    cb <# "update"  $ fun (\_ _ [p] -> update p)
                    cb <# "render"  $ fun (\_ _ [p] -> render p)
                    return cb
                )

  -- Store reference in document
  doc <- jsg "document"
  doc ^. jss "phaser" phaser


-- | Create game
main :: IO ()
main = do
  doc <- jsg "document"
  doc ^. jss "initPhaser" (fun (\_ _ [ele] -> initPhaser ele))
