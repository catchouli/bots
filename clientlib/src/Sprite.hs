module Sprite where

import Prelude hiding ((.))
import Control.Category ((.))
import Data.IORef
import Control.Monad.Reader
import Reactive.Banana
import Reactive.Banana.Frameworks
import Reactive.Banana.Combinators


-- | A sprite reference, with an ID and its state
data Sprite = Sprite { spriteId :: Int
                     , spriteData :: SpriteData
                     } deriving (Show)


-- | Sprite current state e.g. position, animation, etc
data SpriteData = SpriteData { x :: Float
                             } deriving (Show)


data Env = Env { }


-- | Update all sprites, e.g. sync their states to the renderer
updateSprites :: [Sprite] -> IO ()
updateSprites = mapM_ (\(Sprite spriteId spriteData) -> putStrLn $ ("Updating sprite " ++ (show spriteId) ++ ", data: " ++ show spriteData))


-- | Create a sprite generator from a sprite data behavior
spriteGen :: MomentIO (Behavior SpriteData) -> IORef Int -> ReaderT (IORef Int) MomentIO (Int, Behavior Sprite)
spriteGen sprite spriteIds = do
  spriteId <- liftIO $ readIORef spriteIds
  liftIO $ modifyIORef spriteIds (+1)
  spriteData <- lift $ sprite
  return $ (spriteId, Sprite spriteId <$> spriteData)


-- | A player sprite declaration
playerSprite :: Event Float -> Event Float -> IORef Int -> ReaderT (IORef Int) MomentIO (Int, Behavior Sprite)
playerSprite eTick eNewVel = spriteGen $ do
  bVel <- stepper 0 eNewVel
  let bPosChange = (*) <$> bVel <@> eTick
  bPos <- accumB 0 ((+) <$> bPosChange)
  return $ SpriteData <$> bPos


-- | Our test network description
networkDesc :: AddHandler ()
            -> AddHandler ()
            -> AddHandler Float
            -> AddHandler ()
            -> AddHandler Int
            -> AddHandler Float
            -> MomentIO ()
networkDesc hStart hRender hTick hAddSprite hRemoveSprite hNewVel = do
  eStart <- fromAddHandler hStart
  eRender <- fromAddHandler hRender
  eTick <- fromAddHandler hTick
  eAddSprite <- fromAddHandler hAddSprite
  eRemoveSprite <- fromAddHandler hRemoveSprite

  eNewVel <- fromAddHandler hNewVel

  let add x xs = x : xs
  let remove id xs = filter (\(a, _) -> a /= id) xs

  spriteIds <- liftIO $ newIORef (0::Int)

  let sg = runReaderT (playerSprite eTick eNewVel spriteIds) undefined

  eSprites <- do
    defPlayer <- sg
    let defaultSprites = [defPlayer]
    eNewEntry <- execute $ (sg) <$ eAddSprite
    accumE defaultSprites $ unions [ id <$ eStart
                                   , add <$> eNewEntry
                                   , remove <$> eRemoveSprite
                                   ]

  bSprites <- switchB (pure []) (sequenceA . fmap snd <$> eSprites)

  reactimate $ updateSprites <$> bSprites <@ eRender


-- | Our testing framework
test :: IO (IO (), Float -> IO (), IO (), Int -> IO (), Float -> IO ())
test = do
  (hStart, sStart) <- newAddHandler
  (hRender, sRender) <- newAddHandler
  (hTick, sTick) <- newAddHandler
  (hAddSprite, sAddSprite) <- newAddHandler
  (hRemoveSprite, sRemoveSprite) <- newAddHandler

  (hNewVel, sNewVel) <- newAddHandler

  network <- compile $ networkDesc hStart hRender hTick hAddSprite hRemoveSprite hNewVel

  actuate network
  sStart ()

  return $ (sRender (), sTick, sAddSprite (), sRemoveSprite, sNewVel)
